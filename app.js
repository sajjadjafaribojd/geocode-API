/* jshint esversion:6 */

 const yargs=require('yargs');

// //const gecode=require('./geocode/geocode.js'); // same like below.
 const gecode=require('./geocode/geocode');
 const weather=require('./weather/weather');

 const argv=yargs
     .options({
         a:{
             demand:true,
             alias:'address',
             describe:'addres for weather for',
             string:true

         },

       /*s:{     demand:true,
             alias:'sajjad',
             describe:'test for more option ',
             string:true
         }*/
     })
     .help()
     .alias('help','h')
     .argv;

     gecode.geocodeAddress(argv.address,(errorMessage,result)=>{
         if(errorMessage){
             console.log(errorMessage);
         }else{
             console.log(result.address);
             weather.getWeather(result.Latitude,result.Longitude,(errorMessage,weatherResult)=>{
                if(errorMessage){
                    console.log(errorMessage);
                }else{
                    console.log(`It's currently temperature:${weatherResult.temperature}`);
                }
            });

         }
     });

