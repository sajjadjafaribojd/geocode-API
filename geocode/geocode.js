/* jshint esversion:6 */

const request=require('request');

var geocodeAddress=(address,callback)=>{
    var urlEncodeAddress=encodeURIComponent(address);
    //console.log(urlEncodeAddress);
    
request({
    url:`http://open.mapquestapi.com/geocoding/v1/address?key=HJdUJzSFNtpi1WNFvtqwccX8l61KyLc1&location=${urlEncodeAddress}`,
    json:true
},
(error,response,body)=>{
    if(error){
        callback('Unable to connect to Google server');
    } else if(body.statuscode === 1){
        callback("Can not find address");
    }else{
        callback(undefined,{ //is ok same like below
            address:body.results[0].locations[0].adminArea3,
            Latitude:body.results[0].locations[0].latLng.lat,
            Longitude:body.results[0].locations[0].latLng.lng
        });

        /*callback({ // is ok
            address:body.results[0].locations[0].adminArea3,
            Latitude:body.results[0].locations[0].latLng.lat
        });*/
    }
    
    });
};



module.exports.geocodeAddress=geocodeAddress; // create module 3

/*module.exports={ // create modoule 1
    geocodeAddress
};*/ 

/*module.exports={ // create module 2
    geocodeAdd:geocodeAddress
};*/


