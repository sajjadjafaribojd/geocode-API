/* jshint esversion:6 */

const request=require('request');

var getWeather=(lat,lng,callback)=>{
    request({
        url:`https://api.darksky.net/forecast/9ca899962e04021578f0c569919e163b/${lat},${lng}`,
        json:true
    },(error,response,body)=>{
       if(error){
          callback('ubable connect to forecast api server');
       }else if(response.statusCode===400){
          callback('unable to fetch wather.');
       }else if(response.statusCode===200){
           callback(undefined,{
                temperature:body.currently.temperature
           });
       }
    
    });
    
};

module.exports.getWeather=getWeather;

/*module.exports={
    getWeather
}*/